/**
 * Estudante: Willian Alberto Lauber
 * ArrayWriter: Sincroniizacao de threads
 * Exemplo que mostra um problema na
 * escrita compartilhada de um vetor
 * por multiplas threads.
 * 
 * Autor: Lucio Agostinho Rocha
 * Ultima modificacao: 09/08/2017
 */
package exemplo3;

import java.util.*;

public class ArrayWriter implements Runnable {

    private final VetorCompartilhado vetorCompartilhado;
    private int startValue; 
        
    public ArrayWriter(int value, VetorCompartilhado vetor){
        startValue=value;
        vetorCompartilhado=vetor;
    }//fim do construtor
    
    public void run(){
        
        for (int i=startValue; i<startValue+3; i++){
             vetorCompartilhado.add(i); //Adiciona um elemento em ordem crescente no vetor compartilhado
            vetorCompartilhado.sub();
            vetorCompartilhado.add(i+5); //Adiciona um elemento em ordem crescente no vetor compartilhado
            
        }
        
    }//fim run
    
}
