/**
 * ArrayWriter: Sincroniizacao de threads
 * Exemplo que mostra um problema na
 * escrita compartilhada de um vetor
 * por multiplas threads.
 * Estudante: Willian Alberto Lauber
 * Autor: Lucio Agostinho Rocha
 * Ultima modificacao: 09/08/2017
 */
package exemplo3;

import java.util.*;

public class VetorCompartilhado {
    private final ArrayList<Integer> arraylist;
//    private int writeIndex=0; //indice do proximo elemento a ser gravado
    private static Random generator = new Random();
    
    public VetorCompartilhado(int size){
        arraylist = new ArrayList<Integer>();
    }//fim do construtor
    
    public synchronized void  add(int value){
        
//        int position = writeIndex;
        try {
            //Coloca a thread para dormir entre 0 e 500ms
            Thread.sleep(generator.nextInt(500));
        } catch (InterruptedException e){
            e.printStackTrace();
        }
        
        //Insere um valor na posicao do vetor
//        arraylist[position] = value;
        arraylist.add(value);
        System.out.printf("\n%s escreveu %2d na posicao %d", Thread.currentThread().getName(), value,arraylist.size()-1);
        
        //Incrementa o indice
//        writeIndex++;
        System.out.printf("\nProximo indice: %d", arraylist.size());
    }
     public synchronized void  sub(){
        
//        int position = writeIndex;
        try {
            //Coloca a thread para dormir entre 0 e 500ms
            Thread.sleep(generator.nextInt(500));
        } catch (InterruptedException e){
            e.printStackTrace();
        }
        
        //Insere um valor na posicao do vetor
        System.out.printf("\n%s removel %2d na posicao %d", Thread.currentThread().getName(), arraylist.get(arraylist.size()-1), arraylist.size());
        arraylist.remove(arraylist.size()-1);
        
        //Incrementa o indice
        System.out.printf("\nProximo indice: %d", arraylist.size());
    }
    
    //Metodo sobrecarregado
    public String toString(){
        return "\nVetor:\n" + arraylist.toString();
    }
    
}
